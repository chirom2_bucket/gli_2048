import controller.Controller;
import controller.IController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Board;
import model.BoardImpl;

public class Main extends Application {

    private static final String TITLE = "2048 by Jérémie GOAS and Romain CHIQUET";

    private static final int SIZE = 4;


    @Override
    public void start(Stage primaryStage) throws Exception{

        // Load FMXL file (interface layout presentation)

        FXMLLoader loader = new FXMLLoader( getClass().getResource("ressources/game_interface.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle(TITLE);
        primaryStage.setResizable(false);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();

        IController controller = (Controller) loader.getController();

        Board board = new BoardImpl( SIZE );

        controller.setModel(board);

    }


    public static void main(String[] args) {
        launch(args);
    }
}
