package view;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import model.Board;
import model.Tile;

/**
 * Created by goas on 19/10/15.
 */
public class CustomGrid extends GridPane implements View{

    private Board board;

    @Override
    public void setModel(Board board) {
        this.board = board;
    }

    @Override
    public void paint() {
        // Remove all data
        this.getChildren().clear();

        if ( board != null ){

            double width = ( this.getWidth() - (this.getHgap()*(board.getSideSizeInSquares()+1) ) ) / board.getSideSizeInSquares();
            double height = ( this.getHeight() - (this.getVgap()*(board.getSideSizeInSquares()+1) ) ) / board.getSideSizeInSquares();

            // Warning : (indice : 1 to n)
            for ( int i=1; i<=board.getSideSizeInSquares(); i++) {
                for (int j = 1; j <= board.getSideSizeInSquares(); j++) {

                    Tile tile = board.getTile(j,i);

                    if (tile == null) {
                        Label label = new Label();
                        label.setPrefWidth(width);
                        label.setPrefHeight(height);
                        label.setMaxWidth(width);
                        label.setMaxHeight(height);
                        label.setMinWidth(width);
                        label.setMinHeight(height);
                        label.setStyle("-fx-background-color: red;");

                        GridPane.setConstraints(label, i, j);
                        this.getChildren().add(label);
                    } else {

                        Label label = new Label();
                        label.setPrefWidth(width);
                        label.setPrefHeight(height);
                        label.setMaxWidth(width);
                        label.setMaxHeight(height);
                        label.setMinWidth(width);
                        label.setMinHeight(height);
                        label.setStyle("-fx-background-color: green;");
                        label.setFont(Font.font(25));
                        label.setAlignment(Pos.CENTER);
                        label.setText( "" + ((int) Math.pow(2,tile.getRank())) );

                        GridPane.setConstraints(label, i, j);
                        this.getChildren().add(label);

                    }
                }
            }
        }
    }
}
