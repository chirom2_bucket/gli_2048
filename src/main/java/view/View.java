package view;

import model.Board;

/**
 * Created by goas on 19/10/15.
 */
public interface View {

    public void setModel( Board board );

    public void paint();

}
