package controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyEvent;
import model.Board;
import view.View;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements IController, Initializable{

    private Board board;//Model

    @FXML private View grid_layout;

    public boolean win = false;

    public Controller(){}

    /**
     * Get an event and inform the model.
     * Direction will be use to pack tiles into the good direction
     * @param direction pressed by the user
     */
    @Override
    public void receiveEvent(Board.Direction direction) {
        if(direction != null){

            board.packIntoDirection(direction);

            if ( board.randomTileInBoard() ){
                board.commit();
            }

            // Check of the user win
            if ( !win && board.haveFinish() ){
//                Alert alert = new Alert(Alert.AlertType.INFORMATION);
//                alert.setTitle("Information Dialog");
//                alert.setHeaderText(null);
//                alert.setContentText("Vous avez terminé le jeu. Félicitation :)");
//                alert.showAndWait();

                System.out.println("Vous avez terminé le jeu. Mais vous n'avez pas gagné.");

                win = true;
            }

            if ( !board.canContinue() && !win ){
                System.out.printf("Vous avez terminé le jeu. Mais vous n'avez pas gagné.");
//                Alert alert = new Alert(Alert.AlertType.INFORMATION);
//                alert.setTitle("Information Dialog");
//                alert.setHeaderText(null);
//                alert.setContentText("Vous avez terminé le jeu. Mais vous n'avez pas gagné.");
//                alert.showAndWait();
            }

            System.out.println( "End event" );
            grid_layout.paint();
        }
    }

    @Override
    @FXML public void onKeyPressed(KeyEvent event) {
        switch ( event.getCode() ){
            case UP:
                receiveEvent(Board.Direction.TOP);
                break;
            case DOWN:
                receiveEvent(Board.Direction.BOTTOM);
                break;
            case LEFT:
                receiveEvent(Board.Direction.LEFT);
                break;
            case RIGHT:
                receiveEvent(Board.Direction.RIGHT);
                break;
            default:
                // Do nothing
                break;
        }
    }

    @Override
    @FXML
    public void onClickQuitButton() {
        Platform.exit();
    }

    @Override
    @FXML public void onClickRestartButton() {
        board.init();
        win = false;
        grid_layout.paint();
    }

    @Override
    public void setModel(Board board) {
        this.board = board;
        grid_layout.setModel(board);
        board.init();
        grid_layout.paint();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {}


}
