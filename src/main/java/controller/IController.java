package controller;

import javafx.scene.input.KeyEvent;
import model.Board;

/**
 * Created by romain on 13/10/15.
 * Describe an interface for a controller
 */
public interface IController {

    /**
     * Get an event and inform the model.
     * Direction will be use to pack tiles into the good direction
     * @param direction pressed by the user
     */
    public void receiveEvent(Board.Direction direction);

    public void onKeyPressed( KeyEvent event );

    public void onClickQuitButton();

    public void onClickRestartButton();

    public void setModel(Board board);
}
